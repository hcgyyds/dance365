import request from '@/utils/request'
let token = 'a57a9a6f-4db3-410a-b156-15492fb8f863';

export interface GroupTypeModel {
  channel: string,
  createTime: number,
  data: {
    total_price: number,
    time_limit: boolean,
    vip_grade: number,
    end_time: string,
    title: string,
    duration: number,
    apple_priceId: string,
    cover: string,
    start_time: string,
    unit: string,
    renew_time: string,
    price: number,
    isYouthOnly?: boolean,
    recom: boolean,
    activitySwitch?: boolean,
    saving?: number
  },
  groupName: string,
  groupType: string,
  id: string,
  sortNum: number,
  status: string,
  subGroupName: string,
  subGroupType: string,
  updateTime: number,
  version: string
}

export default {
  // 获取会员组
  getGroupType() {
    return request.get<any, GroupTypeModel[]>(`/members`)
  }
}