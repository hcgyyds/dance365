import request from "@/utils/request";

export default {
  getInCityData() {
    return request.get<any, null>("/city");
  },
  getSkillData(){
    return request.get<any, null>("/information");
  }

};
