import request from "@/utils/request";
import mock from "@/utils/courseMock";

export default {
    //获取会员专区/视频课分类属性列表数据
    reqSortList() {
        return mock.get<any, any>("/moment/properties/filter/vip_zone?access_token=c494ae44-3adc-48ca-8749-5128a53358d7");
    },
    //获取直播课分类属性列表
    reqLiveList() {
        return mock.get<any, any>("/moment/properties/filter/live_course?access_token=c494ae44-3adc-48ca-8749-5128a53358d7");
    },
    //获取线下课分类属性列表
    reqOfflineList() {
        return mock.get<any, any>("/moment/properties/filter/live_course?access_token=c494ae44-3adc-48ca-8749-5128a53358d7");
    },
    //获取主题页面图片的数据
    reqmotif() {
        return mock.get<any, any>("/moment/activityZones/collect/default?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=16");
    },
    // 获取会员专区视频详情数据
    postVideo() {
        return mock.post<any, any>(`/moment/moments/collect/vip_zone/integrated?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`);
    },
    // 获取视频课视频详情数据
    videoList() {
        return mock.post<any, any>(`/moment/moments/collect/video_course/integrated?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`);
    },
    // 获取直播课视频详情数据
    postLiveVideo() {
        return mock.post<any, any>(`moment/moments/collect/live_course/integrated?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`);
    },
    // 获取线下课视频详情数据
    postOfflineList() {
        return mock.post<any, any>(`/moment/moments/collect/video_course/integrated?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`);
    },
    // 获取vip销售量数据
    vipSales() {
        return mock.post<any, any>(`moment/moments/collect/vip_zone/sellCountDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageNum=0`);
    },
    // 获取vip浏览量
    postViewList() {
        return mock.post<any, any>(`moment/moments/collect/vip_zone/viewCountDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`);
    },
    // 获取vip价格由高到低
    priceHighList() {
        return mock.post<any, any>(`moment/moments/collect/vip_zone/priceDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`);
    },
    // 获取vip发布时间数据
    addTimeList() {
        return mock.post<any, any>(`moment/moments/collect/video_course/priceDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`);
    },
    // 获取视频课评价数据
    VideoAppraiseList() {
        return mock.post<any, any>(`moment/moments/collect/video_course/favorableValueDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`);
    },
    Video() {
        return mock.post<any, any>(`/moment/moments/collect/video_course/sellCountDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`);
    },
};
