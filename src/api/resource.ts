import request from '@/utils/request'
let token = 'a57a9a6f-4db3-410a-b156-15492fb8f863';

export interface DynamicDataModel {
  content: ContentModel[],
  first: boolean,
  last: boolean,
  number: number,
  numberOfElements: number,
  offset: null,
  size: number,
  sort: SortModel[],
  statistics: null,
  totalElements: number,
  totalPages: number,
  useOffset: boolean
}

export interface ContentModel {
  enclosureCount: number,
  visiblePermissions: string,
  subject: string,
  num: number,
  channel: string,
  shelfTime?: number,
  cityId: string,
  securityStatus: number,
  moreBackup: {
    richText: string,
    photoCount?: number,
    detail?: {}
  },
  title: string,
  processTime: number,
  cover: string[],
  mediaSummary: {
    count?: number | null,
    duration?: number | null
  },
  router: string,
  commodityBackup: {
    unitPrice: string,
    price: string,
    originPrice: string,
    showLinePrice: boolean,
    commissionAmount?: null,
    commissionRate?: null,
    commissionType?: null,
  },
  cityName: string,
  recentFavorableValue: number,
  hasLiveBroadcast: number,
  id: string,
  targetStatus: string,
  recDegree: number,
  creator: string,
  aiyigeApplication: string,
  wordFilterStatus: string,
  creatorBackup: {
    level: number,
    name: string,
    avatar: string,
    id: string,
    vipGrade: number,
    accid?: string,
    certificationType?: null,
    mobile?: null,
    recentSellerFavorableValue?: number,
    recomFactor?: null,
    recomFactorExpireTime?: null,
    sellerFavorableValue?: number,
    type?: null,
    vipExpireTime?: null,

  },
  likabilityDegree: number,
  editHighestVersion: string,
  relevancyAvocations: string[],
  updateTime: number,
  forceUpdate: number,
  classification: string,
  version: number,
  tags: TagModel[],
  favorableValue: number,
  needPromotion: number,
  freshnessDegree: number,
  onsellUpdateTime: number,
  auditTime: number | null,
  createTime: number,
  addressChanged: boolean,
  statisticsBackup: statisticsBackupModel,
  webUrl: string,
  addressConfirmRemind: boolean,
  initExposureNum: number,
  momentClassificationBackup: {
    workTitle: string,
    classification: string,
    originalStatement?: number,
  },
  onsellTime: number,
  status: string,
  remark?: string,
  auditorName?: string,
  auditor?: string,
  auditMsg?: [],
  auditStatus?: null,
  courseEnded?: null,
  depositAmount?: number,
  detailBackup: {
    content: [],
    guarantee: []
  },
  divisionBackup?: null,
  enclosures?: [],
  esSyncStatus?: null,
  esid?: null,
  esidBackup?: null,
  imageTitleBackup?: null,
  inventoryBackup?: null,
  letBuy?: null,
  liveBroadcast?: null,
  liveBroadcastTitles?: [],
  mineBackup?: {
    accessoryRequired: number,
    buyed: number,
    commented: number,
    favoriteId: null,
    iosDeviceVip: number,
    letInLiveCourse: number,
    liveCourseRequired: number,
    majorCourseRequired: number,
    musicRequired: number,
    mutualFollower: null,
    praiseId: null,
    resourceFrom: null,
    roomRole: null,
    userFollowId: null,
    videoCourseRequired: number
  },
  onSalesBackup?: null,
  priorityDisplay?: null,
  priorityDisplayTime?: null,
  rand?: null,
  recommendActivityAndTopic?: [],
  recommendMineBackup?: [],
  recommendOtherBackup?: SubjectModel[],
  repeatRequest?: null,
  requireBackup?: RequireModel[],
  securityAuditResult?: null,
  studyProgressBackup?: [],
  subjectTag?: null,
  summary?: null,
  ticket?: null,
  topicQuoteInfos?: [],
  vipTag?: null,
  vipTitleBackup?: null,

}

interface RequireModel {
  requireDesc: string,
  requireType: string
}

interface statisticsBackupModel {
  exposureCount: number,
  enquiryCount: number,
  lockCount: number,
  studentCount: number,
  playBackDuration: number,
  praiseCount: number,
  requireVideoCourseCount: number,
  requireMusicCount: number,
  shareAmount: number,
  commentCount: number,
  shareCount: number,
  requireAccessoryCount: number,
  reviewCount: number,
  sellAmount: number,
  signupCount: number,
  requireLiveCourseCount: number,
  sellCount: number,
  viewCount: number,
  actualSellCount: number,
  liveStatus: number,
  favoriteCount: number,
  requireMajorCourseCount: number,
  userCacheCount: number
}

interface SubjectModel {
  cover: string[],
  router: string,
  subject: string,
  statisticsBackup: statisticsBackupModel,
  id: string,
  title: string,
  momentClassificationBackup: {
    workTitle: string,
    classification: string
  }
}
export interface TagModel {
  groupType: string,
  name: string,
  id: string
}

export interface SortModel {
  ascending: boolean,
  descending: boolean,
  direction: string,
  ignoreCase: boolean,
  nullHandling: string,
  property: string
}

export interface DynamicParams {
  pageSize: number,
  pageNum: number,
  classification: string,
  status: number
}


export default {
  // 获取数据
  getDynamicData({ pageSize, pageNum = 0, classification, status }: DynamicParams) {
    return request.get<any, DynamicDataModel>(`/dynamics`)
  },
  // 删除
  deleteData(id: string) {
    return request.delete<any, null>(`/moment/moments/${id}?access_token=${token}`)
  },
  // 取消发布
  unPublish(id: string) {
    return request.patch(`/moment/moments/${id}/action/getoff?access_token=${token}`)
  },
  // 发布相关
  publish(publishType: string,
    momentId: string) {
    return request.post(`/userCenter/userPublishEquitys/action/editCheckUserPublishEquity?access_token=${token}`, { publishType, momentId })
  },
  puton(momentId: string) {
    return request.patch(`/moment/moments/${momentId}/action/puton?access_token=${token}`)
  },
  // 编辑
  edit(momentId: string) {
    return request.get<any, ContentModel>(`/moment/moments/${momentId}?access_token=${token}`)
  }
}