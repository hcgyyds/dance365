import request from "@/utils/request";
enum userApi {
  login = `/user/passport/login`,
  register = `/user/passport/register`,
  logout = `/user/passport/logout`,
}

export interface userInfoParams {
  password: string;
  phone: string;
}
export interface loginData {
  token: string;
}

export default {
  // 登录
  reqLogin(formData: userInfoParams) {
    return request.post<any, loginData>(userApi.login, formData);
  },
  // 注册
  reqRegister(formData: userInfoParams) {
    return request.post<any, null>(userApi.register, formData);
  },

  // 退出登录
  reqLogOut() {
    return request.get<any, null>(userApi.logout);
  },
};
