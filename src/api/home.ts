// import request from "@/utils/request";
import request from "@/utils/request";

// 这是作品小卡片的接口
//  /moment/moments/collect/original/integrated?access_token=c7777b6e-9ea1-41fb-8264-e357bae7dd79&pageSize=20&pageNum=0&column=original
// 这是爵士舞小卡片的接口
//  /moment/moments/rec/default?access_token=c7777b6e-9ea1-41fb-8264-e357bae7dd79&column=402881ed649d628e01649d636ea70057&pageSize=20&pageNum=0&localCache=20
// 这是动态小卡片的接口
//  /moment/moments/frontDynamic/default?access_token=c7777b6e-9ea1-41fb-8264-e357bae7dd79&pageSize=20&pageNum=0&column=frontDynamic
// 这是专题小卡片的接口
//  /moment/moments/collect/original/integrated?access_token=c7777b6e-9ea1-41fb-8264-e357bae7dd79&pageSize=20&pageNum=0&column=specialTopic
// 这是教程小卡片的接口
//  /moment/moments/collect/course/integrated?access_token=c7777b6e-9ea1-41fb-8264-e357bae7dd79&pageSize=20&pageNum=0&column=course
// 这是推荐小卡片的接口
// /moment/moments/rec/default?access_token=c7777b6e-9ea1-41fb-8264-e357bae7dd79&column=recommend&pageSize=20&pageNum=0&localCache=0
// 这是轮播图的
// https://www.dance365.com/apis/promotion/ads/search/findByPosition?access_token=c7777b6e-9ea1-41fb-8264-e357bae7dd79&position=pc_mainpage
// 这是个人资料携带的参数
// https://www.dance365.com/apis/moment/properties/filter/original?access_token=c7777b6e-9ea1-41fb-8264-e357bae7dd79

// 轮播图的泛型
// interface BannerListModel {
//   images: BannerModel[];
// }
//视频列表类
export interface VideoModel {
  cover: string;
  duration: number;
  persistentId: string;
  size: number;
  originUrl: string;
  free: string;
  title: string;
  url: string;
  hlsUrl: string;
}

export interface MoreBackupModel {
  videoCount: number;
  totalDuration: number;
  videos: VideoModel[];
}

export interface MediaSummaryModel {
  duration: number;
  count: number;
}

export interface CreatorBackupModel {
  level: number;
  name: string;
  avatar: string;
  id: string;
  vipGrade: number;
  certificationType: string;
}

export interface StatisticsBackupModel {
  exposureCount: number;
  enquiryCount: number;
  lockCount: number;
  studentCount: number;
  playBackDuration: number;
  praiseCount: number;
  requireVideoCourseCount: number;
  requireMusicCount: number;
  shareAmount: number;
  commentCount: number;
  shareCount: number;
  requireAccessoryCount: number;
  reviewCount: number;
  sellAmount: number;
  signupCount: number;
  requireLiveCourseCount: number;
  sellCount: number;
  viewCount: number;
  actualSellCount: number;
  liveStatus: number;
  favoriteCount: number;
  requireMajorCourseCount: number;
  userCacheCount: number;
}

export interface MomentClassificationBackupModel {
  workTitle: string;
  classification: string;
}

export interface ContentModel {
  enclosureCount: number;
  moreBackup: MoreBackupModel;
  notVipPromotion: number;
  subject: string;
  num: number;
  channel: string;
  cityId: string;
  title: string;
  cover: string[];
  router: string;
  mediaSummary: MediaSummaryModel;
  cityName: string;
  avocationTags: [];
  id: string;
  recDegree: number;
  creator: string;
  algorithmSortValue: number;
  aiyigeApplication: string;
  vipPromotion: number;
  creatorBackup: CreatorBackupModel;
  editHighestVersion: string;
  updateTime: number;
  version: number;
  tags: [];
  needPromotion: number;
  onsellUpdateTime: number;
  createTime: number;
  statisticsBackup: StatisticsBackupModel;
  initExposureNum: number;
  momentClassificationBackup: MomentClassificationBackupModel;
  status: number;
  onsellTime: number;
}

export interface VideoListModel {
  content: ContentModel[];
  first: boolean;
  last: boolean;
  number: number;
  numberOfElements: number;
  offset: null;
  size: 20;
  sort: null;
  statistics: null;
  totalElements: number;
  totalPages: number;
  useOffset: boolean;
}

//筛选类
export interface ScreenModel {
  name: string;
  id: string;
}

export interface ProductionScreenModel {
  choises: ScreenModel[];
  label: string;
  name: string;
  style: string;
  valueType: string;
}
// 兴趣
export interface HobbyModel {
  groupType: string;
  name: string;
  id: string;
  iconShow: boolean;
}
//
const access_token = "2d118c47-0429-4b79-88f7-94fb0df5002e";
export default {
  // 推荐的小卡片请求
  reqRecommend() {
    return request.get<any, VideoListModel>(`/recommend/card`);
  },
  // 作品筛选的请求
  reqProductionScreen() {
    return request.get<any, VideoListModel>("/filter/original");
  },
  //
  getHobbyList() {
    return request.get<any, HobbyModel[]>(`/avocation`);
  },
};
