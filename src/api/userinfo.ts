import request from '@/utils/request'
let token = 'a57a9a6f-4db3-410a-b156-15492fb8f863'

interface AvocationModel {
  groupType: string,
  name: string,
  id: string
}

export interface UserInfoModel {
  accessToken: null,
  account: string,
  audioExtractTimes: null,
  authorities: [
    {
      authority: string
    }
  ],
  avatar: string,
  avocation: AvocationModel[],
  avocationString: string,
  certificationBackup: null,
  certificationName: null,
  certificationType: null,
  channel: AvocationModel,
  channelId: string,
  cityNameAlgorithm: null,
  createTime: number,
  editReviewBackup: {
    slideBackup: [],
    avatar: string,
    account: string
  },
  equity: {
    comments: boolean,
    audioBeat: boolean,
    majorCourseOnLineRefund: boolean,
    publishSpecialTopic: boolean,
    createLiveCourse: boolean,
    createBroadcast: boolean,
    talk: boolean,
    login: boolean,
    publishTest: boolean
  },
  exp: null,
  hasWalletPassword: boolean,
  id: string,
  level: number,
  mineBackup: {
    mutualFollower: null,
    userFollowId: null
  },
  miniAvatar: null,
  mobile: boolean,
  mobileAscription: null,
  mobileVerified: number,
  moreBackup: {
    address: null,
    bindQQ: boolean,
    bindSina: boolean,
    bindTiktok: null,
    bindToutiao: null,
    bindWeixin: boolean,
    birthDate: null,
    city: null,
    cityName: null,
    country: null,
    domain: null,
    email: null,
    id: string,
    identityCard: null,
    introduce: null,
    invitationCode: string,
    liveCourseFavorableValue: number,
    majorCourseFavorableValue: number,
    promotionChannelTypeId: string,
    recentRepeatRate: number,
    recentSellerFavorableValue: number,
    recentUseTime: number,
    registerChannelName: string,
    registerSource: string,
    repeatRate: number,
    sellerFavorableValue: number,
    sex: string,
    skillLevel: number,
    skillLevelName: string,
    telphone: null,
    webUrl: string,
    zipCode: null
  },
  needAudit: boolean,
  needAuditCreator: null,
  needAuditPatchTime: null,
  newUser: boolean,
  nextActivityDisplayTime: null,
  onlinePlanId: null,
  openWechatRemind: number,
  originObject: boolean,
  password: string,
  router: string,
  salesRevenue: boolean,
  selectedAvocation: number,
  slideBackup: [],
  statisticsBackup: {
    browseCount: number,
    consumptionAmount: number,
    couponCount: number,
    dynamicCount: number,
    fansCount: number,
    favoriteCount: number,
    followCount: number,
    followersCount: number,
    interestCount: number,
    inviteVipDays: number,
    inviterCount: number,
    lastLoginTime: number,
    loginTimes: number,
    messageCount: number,
    oaProductCount: number,
    onlineDynamicCount: number,
    orderAmount: number,
    orderCount: number,
    productCount: number,
    promoterCommission: number,
    purseBalance: number,
    salesAmount: number,
    sellCount: number,
    transactionAmount: number,
    userP: null,
    userSkillLevelA1: null,
    userSkillLevelA2: number,
    viewCount: number,
    waitPayCount: number
  },
  status: number,
  supplierTime: null,
  thirdOauthBackup: null,
  token: null,
  type: string,
  updateTime: number,
  useLogBackup: null,
  userAccidPassword: {
    accid: string,
    createTime: number,
    id: string,
    token: string,
    updateTime: number,
    userId: string
  },
  userChangeInfo: null,
  userDepositBackup: null,
  userOnline: number,
  vipBackup: null,
  vipExpireTime: null,
  vipGrade: number,
  vipPayAgeLimit: boolean,
  walletPassword: null
}

export default {
  getUserInfo() {
    return request.get<any, UserInfoModel>(`/userinfo`)
  }
}