import Mock from "mockjs";
import RecommendData from "./recommendData.json";
import ProductionSynthesizeData from "./productionSynthesizeData.json";
import RecommendHobbyData from "./recommendHobbyData.json";

Mock.mock(`/mock/recommend/card`, () => {
  return {
    code: 200,
    data: RecommendData,
    message: "success",
  };
});
Mock.mock(`/mock/filter/original`, () => {
  return {
    code: 200,
    data: ProductionSynthesizeData,
    message: "success",
  };
});
Mock.mock(`/mock/avocation`, () => {
  return {
    code: 200,
    data: RecommendHobbyData,
    message: "success",
  };
});
