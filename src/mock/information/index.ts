import Mock from 'mockjs'
import city from '../centerMock/data/city.json'
import skill from '../centerMock/data/skillevel.json'

// city
Mock.mock('/mock/city',{
  code:200,
  data:city,
  msg:'ok',
})
// skill
Mock.mock('/mock/information',{
  code:200,
  data:skill,
  msg:'ok',
})

// 