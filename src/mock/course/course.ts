// @ts-ignore
import Mock from "mockjs";
// @ts-ignore
import course from "./index.json";
// @ts-ignore
import course1 from "./index2.json";
// @ts-ignore
import course2 from "./index3.json";
// @ts-ignore
import page from "./page.json";
// @ts-ignore
import page1 from "./page1.json";
// @ts-ignore
import page2 from "./page2.json";
// @ts-ignore
import page3 from "./page3.json";
// @ts-ignore
import page4 from "./page4.json";
// @ts-ignore
import page5 from "./page5.json";
// @ts-ignore
import page6 from "./page6.json";
// @ts-ignore
import page7 from "./page7.json";
// @ts-ignore
import page8 from "./page8.json";
// @ts-ignore
import page9 from "./page9.json";
// @ts-ignore
import page10 from "./page10.json";
// @ts-ignore
import video from "./video.json";
// @ts-ignore
import video2 from "./video2.json";
// @ts-ignore
import video3 from "./video3.json";
// @ts-ignore
import video4 from "./video4.json";
// @ts-ignore
import video5 from "./video5.json";
// @ts-ignore
import video6 from "./video6.json";
// @ts-ignore
import video11 from "./video11.json";
// @ts-ignore
import video12 from "./video12.json";
// @ts-ignore
import video13 from "./video13.json";
// @ts-ignore
import video14 from "./video14.json";
// @ts-ignore
import video15 from "./video15.json";

let token = "access_token=c494ae44-3adc-48ca-8749-5128a53358d7";
Mock.mock(`/mock/moment/properties/filter/vip_zone?access_token=c494ae44-3adc-48ca-8749-5128a53358d7`, "get", () => {
    return {
        code: 200,
        data: course,
        message: "success",
    };
});
Mock.mock(`/mock/moment/properties/filter/live_course?access_token=c494ae44-3adc-48ca-8749-5128a53358d7`, "get", (url: any) => {
    return {
        code: 200,
        data: course2,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/vip_zone/integrated?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: page,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/live_course/integrated?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: page1,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/video_course/integrated?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: page2,
        message: "success",
    };
});
Mock.mock(`/mock/moment/activityZones/collect/default?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=16`, "get", (url: any) => {
    return {
        code: 200,
        data: page3,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/vip_zone/sellCountDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: page4,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/vip_zone/viewCountDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: page5,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/vip_zone/priceDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: page6,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/vip_zone/priceAsc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: page7,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/vip_zone/onsellTimeDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: page8,
        message: "success",
    };
});
Mock.mock(`/mock/moment/properties/filter/vip_zone?access_token=c494ae44-3adc-48ca-8749-5128a53358d7`, "get", (url: any) => {
    return {
        code: 200,
        data: page9,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/vip_zone/sellCountDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: page10,
        message: "success",
    };
});
Mock.mock(`/mock/moment/properties/filter/video_course?access_token=c494ae44-3adc-48ca-8749-5128a53358d7`, "get", (url: any) => {
    return {
        code: 200,
        data: course1,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/video_course/sellCountDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: video,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/video_course/favorableValueDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: video2,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/video_course/viewCountDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: video3,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/video_course/priceDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: video4,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/video_course/priceAsc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: video5,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/video_course/onsellTimeDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: video6,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/live_course/sellCountDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: video11,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/live_course/viewCountDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: video12,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/live_course/priceDesc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: video13,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/live_course/priceAsc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: video14,
        message: "success",
    };
});
Mock.mock(`/mock/moment/moments/collect/live_course/startTimeAsc?access_token=c494ae44-3adc-48ca-8749-5128a53358d7&pageSize=20&pageNum=0`, "post", (url: any) => {
    return {
        code: 200,
        data: video15,
        message: "success",
    };
});
