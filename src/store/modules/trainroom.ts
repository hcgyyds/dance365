import { defineStore } from 'pinia';
import moment from 'moment';
import trainroomApi, {
    type allshowpage,
    type collect,
    type initdataAll,
    type showhistoryall,
} from '../../api/trainmock';

//定义初始化的数据--缓存
const initshowcache = (): allshowpage => {
    return {
        content: [],
        first: true,
        last: true,
        number: 0,
        numberOfElements: 0,
        size: 0,
        sort: null,
        totalElements: 0,
        totalPages: 0,
    };
};

//定义初始化的数据--收藏

const initshowcollect = (): collect => {
    return {
        content: [],
        first: true,
        last: true,
        number: 0,
        numberOfElements: 0,
        offset: null,
        size: 0,
        sort: null,
        statistics: null,
        totalElements: 0,
        totalPages: 0,
        useOffset: true,
    };
};

//定义接收参数的类型
const useTrainroomStore = defineStore('userInfo', {
    state() {
        return {
            token: '59397b7d-705b-4984-830c-f891bfb354c9',
            initdata: <initdataAll>{}, //初始化,全部数据
            showcachedata: initshowcache(), //缓存拿到的数据
            showcollectdata: initshowcollect(), //收藏拿到的数据
            historydataList: <showhistoryall>{}, //历史浏览记录
            mytraindatalist: <initdataAll>{}, //练习室其他数据
        };
    },
    actions: {
        //进入页面初始化
        // async getinitTrain() {
        //     try {
        //         let result = await trainroomApi.inittrain();
        //     } catch (error) {
        //         return Promise.reject(error);
        //     }
        // },
        //进入页面初始化数据 --全部
        async getinittraindata() {
            try {
                let result = await trainroomApi.inittraindata();
                this.initdata = result;
            } catch (error) {
                return Promise.reject(error);
            }
        },
        //点击右上发送请求 --缓存
        async getshowpage() {
            try {
                let result = await trainroomApi.getshowcache();

                this.showcachedata = result;
            } catch (error) {
                return Promise.reject(error);
            }
        },
        //点击右上发送请求--收藏
        async getshowcollect() {
            try {
                let result = await trainroomApi.getshowcollect();

                this.showcollectdata = result;
            } catch (error) {
                return Promise.reject(error);
            }
        },

        //点击右上发送请求--历史
        async gethistorydata() {
            try {
                let result = await trainroomApi.gethistorydata();
                this.historydataList = result;
            } catch (error) {
                return Promise.reject(error);
            }
        },

        //点击练习室
        async getmytraindata(myListCategory: string) {
            try {
                let result = await trainroomApi.getmytrainroomdata();
                this.mytraindatalist = result;
            } catch (error) {
                return Promise.reject(error);
            }
        },
    },
    getters: {
        //初始化数据
        initdatacom: state => {
            return (state.initdata.content || []).map(item => {
                return {
                    name: item.planName, //标题
                    pic: item.cover, //图片
                    count: item.weekTrainedCount, //练习几次
                    time: item.weekTrainedDuration, //长时间
                    deleteFlag: item.deleteFlag,
                    id: item.id,
                    url: '',
                    createTime: '', //创建时间
                    authorname: '', //作者名
                    avatar: '', //作者头像
                    router: '', //视频
                    updateTime: '', //昨天时间
                };
            });
        },

        // 缓存数据
        showcachedatacon: state => {
            return (state.showcachedata.content || []).map(item => {
                return {
                    name: item.title, //标题
                    pic: item.cover, //图片
                    url: item.videos.map(url => url.url),
                    id: item.id,
                    createTime: moment(item.createTime).format('MM-DD'), //创建时间
                    count: 0, //练习几次
                    time: '', //长时间
                    deleteFlag: '',
                    authorname: '', //作者名
                    avatar: '', //作者头像
                    router: '', //视频
                    updateTime: '', //昨天时间
                };
            });
        },

        //收藏数据
        showcollectdatacon: state => {
            return state.showcollectdata.content.map(item => {
                return {
                    name: item.momentTitle, //标题
                    pic: item.momentBackup.cover[0], //图片
                    id: item.id,
                    router: item.momentBackup.router, //视频
                    authorname: item.momentBackup.creatorBackup.name, //作者名
                    avatar: item.momentBackup.creatorBackup.avatar, //作者头像
                    createTime: moment(item.createTime).format('MM-DD'), //创建时间
                    count: 0, //练习几次
                    time: '', //长时间
                    deleteFlag: '',
                    url: [''],
                    updateTime: '', //昨天时间
                };
            });
        },

        //历史数据
        historydatacon: state => {
            return state.historydataList.content?.map(item => {
                return {
                    name: item.momentTitle, //标题
                    pic: item.momentBackup.cover[0], //视频图片
                    authorname: item.momentBackup.creatorBackup.name, //作者名
                    avatar: item.momentBackup.creatorBackup.avatar, //作者头像
                    updateTime: moment(item.updateTime).subtract(1, 'days').calendar(), //昨天时间
                    id: item.id, //id
                    router: item.momentBackup.router, //视频
                    createTime: moment(item.createTime).format('MM-DD'), //创建时间
                    count: 0, //练习几次
                    time: '', //长时间
                    deleteFlag: '',
                    url: [''],
                };
            });
        },
    },
});

export default useTrainroomStore;
