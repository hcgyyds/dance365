import loginApi from "@/api/login";
import { defineStore } from "pinia";
import { ElMessage } from "element-plus";

const userRegisterInfo = defineStore("register", {
  state() {
    return {
      register: {},
    };
  },
  actions: {
    async getUserRegister(formData: any) {
      try {
        let res = await loginApi.reqRegister(formData);
        console.log(res);
      } catch (error) {
        ElMessage.error("请求失败");
      }
    },
  },
  getters: {},
});

export default userRegisterInfo;
