import { defineStore } from "pinia";
import userinfoApi from "@/api/userinfo";
import type UserInfoModel from "@/api/userinfo";

const useUserInfoStore = defineStore("userInfo", {
  state() {
    return {
      userInfo: {},
    };
  },
  actions: {
    async getUserInfo() {
      try {
        let result = await userinfoApi.getUserInfo();
        this.userInfo = result;
      } catch (error) {
        return Promise.reject(error);
      }
    },
  },
  getters: {},
});
export default useUserInfoStore;
