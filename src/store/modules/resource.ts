import { defineStore } from 'pinia'
import resourceApi, { type DynamicDataModel, type DynamicParams } from '@/api/resource'

const initDynamicData = (): DynamicDataModel => ({
  content: [],
  first: true,
  last: true,
  number: 0,
  numberOfElements: 0,
  offset: null,
  size: 0,
  sort: [
    {
      ascending: false,
      descending: true,
      direction: "",
      ignoreCase: false,
      nullHandling: "",
      property: ""
    }
  ],
  statistics: null,
  totalElements: 0,
  totalPages: 0,
  useOffset: false
})

interface PublishParams {
  publishType: string,
  momentId: string
}

const useResourceStore = defineStore('resource', {
  state() {
    return {
      dynamicData: initDynamicData()
    }
  },
  actions: {
    // 获取资源动态数据
    async getDynamicData(params: DynamicParams) {
      try {
        let result = await resourceApi.getDynamicData(params)
        this.dynamicData = result
      } catch (error) {
        return Promise.reject(error)
      }
    },
    // 删除数据
    async deleteData(id: string) {
      try {
        await resourceApi.deleteData(id)
      } catch (error) {
        return Promise.reject(error)
      }
    },
    // 取消发布
    async unPublish(id: string) {
      try {
        await resourceApi.unPublish(id)
      } catch (error) {
        return Promise.reject(error)
      }
    },
    // 发布
    async publish({publishType, momentId}: PublishParams) {
      try {
        await resourceApi.publish(publishType, momentId)
        await resourceApi.puton(momentId)
      } catch (error) {
        return Promise.reject(error)
      }
    }
  },
  getters: {

  }
})

export default useResourceStore