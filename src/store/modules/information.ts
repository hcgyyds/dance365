import { defineStore } from "pinia";
import informatioApi from "@/api/information";
const informationStore = defineStore("information", {
  state() {
    return {
      city: [],
      Skill: {},
      newName: "",
    };
  },
  actions: {
    async getCity() {
      try {
        let result = await informatioApi.getInCityData();
        console.log("111", result);
        // @ts-ignore
        this.city = result;
      } catch (error) {}
    },
    async getSkill() {
      try {
        let result = await informatioApi.getSkillData();
        // @ts-ignore
        this.Skill = result;
      } catch (error) {}
    },
    async getName(name) {
      this.newName = name;
    },
  },
  getters: {},
});
export default informationStore;
