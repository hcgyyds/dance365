import { createRouter, createWebHistory } from 'vue-router'
import routes from './routes'

// import useUserInfoStore from '@/store/modules/userInfo'
// import userBaseInfo from '@/store/modules/login'
// const userInfoStore = useUserInfoStore()
// const baseInfo = userBaseInfo()
const router = createRouter({
  history: createWebHistory(),
  routes: routes,
})

const blackList = ['interflow','center','mine']

// router.beforeEach( async (to, from, next) => {
//   let token = JSON.parse(localStorage.getItem('TOKEN') as string)
//   if (token) {
//     let name = JSON.parse(localStorage.getItem('USERINFO') as string).account
//     if(name) {
//       next()
//     } else {
//       try {
//         await userInfoStore.getUserInfo()
//         next(to)
//       } catch (error) {
//         // 清除token
//         await baseInfo.reset();
//         // 跳转登录页
//         next('/login')
//       }
//     }
//   } else {
//     if (blackList.some(path => to.path.includes(path))) {
//       next(`/login?redirect=${to.path}`)
//     } else {
//       next();
//     }
//   }
// })

export default router