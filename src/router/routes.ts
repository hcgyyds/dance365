import type { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
    //  主页
    {
        path: '/home',
        component: () => import('@/pages/home/index.vue'),
        meta: {
            name: '资源',
            type: 'home',
        },
        children: [
            {
                path: '/home/recommend',
                component: () => import('@/pages/home/card/index.vue'),
            },
            {
                path: '/home/original',
                component: () => import('@/pages/home/screen/index.vue'),
            },
            {
                path: '/home',
                redirect: '/home/recommend',
            },
        ],
    },
    {
        path: '/detail/video',
        component: () => import('@/pages/home/videoDesc/index.vue'),
    },
    // [我的]外部
    {
        path: '/mine',
        name: 'Mine',
        component: () => import('@/pages/mine/index.vue'),
        meta: {
            name: '头像',
            type: 'mine',
        },
    },
    // 登录页
    {
        path: '/login',
        name: 'Login',
        component: () => import('@/pages/login/index.vue'),
    },
    // 注册页
    {
        path: '/register',
        component: () => import('@/pages/register/index.vue'),
    },
    // 个人中心
    {
        path: '/center',
        component: () => import('@/pages/center/index.vue'),
        meta: {
            name: '我的',
            type: 'center',
        },
        children: [
            {
                path: '/center/dynamics',
                name: 'Dynamics',
                component: () =>
                    import('@/pages/center/components/rightView/components/resource/index.vue'),
                meta: {
                    name: '资源动态',
                    type: 'dynamics',
                },
            },
            {
                path: '/member',
                name: 'Member',
                component: () =>
                    import('@/pages/center/components/rightView/components/member/index.vue'),
                meta: {
                    name: '我的会员',
                    type: 'member',
                },
            },
            {
                path: '/center/wallet',
                name: 'Wallet',
                component: () =>
                    import('@/pages/center/components/rightView/components/wallet/index.vue'),
                meta: {
                    name: '我的钱包',
                    type: 'wallet',
                },
            },
            {
                path: '/center/coupon',
                name: 'Coupon',
                component: () =>
                    import('@/pages/center/components/rightView/components/coupon/index.vue'),
                meta: {
                    name: '我的优惠券',
                    type: 'coupon',
                },
            },
            {
                path: '/center/order',
                name: 'Order',
                component: () =>
                    import('@/pages/center/components/rightView/components/order/index.vue'),
                meta: {
                    name: '我买的订单',
                    type: 'order',
                },
            },
            {
                path: '/center/goods',
                name: 'Goods',
                component: () =>
                    import('@/pages/center/components/rightView/components/goods/index.vue'),
                meta: {
                    name: '我卖的',
                    type: 'goods',
                },
            },
            {
                path: '/center/information',
                name: 'Information',
                component: () =>
                    import('@/pages/center/components/rightView/components/information/index.vue'),
                meta: {
                    name: '我的资料',
                    type: 'information',
                },
            },
            {
                path: '/center',
                redirect: '/center/dynamics',
            },
        ],
    },
    // 课程和会员
    {
        path: '/course',
        component: () => import('@/pages/course/index.vue'),
        meta: {
            name: '课程',
            type: 'course',
        },
        children: [
            {
                name: 'Vip',
                path: '/course/vip',
                component: () => import('@/pages/course/components/Vip/index.vue'),
            },
            {
                name: 'Live',
                path: '/course/Live',
                component: () => import('@/pages/course/components/Live/index.vue'),
            },
            {
                name: 'Offline',
                path: '/course/Offline',
                component: () => import('@/pages/course/components/Offline/index.vue'),
            },
            {
                name: 'Video',
                path: '/course/Video',
                component: () => import('@/pages/course/components/Video/index.vue'),
            },
            {
                name: 'Theme',
                path: '/course/Theme',
                component: () => import('@/pages/course/components/Theme/index.vue'),
            },
            {
                path: '/course',
                redirect: '/course/vip'
            }
        ],
    },
    //练习室
    {
        path: '/trainroom',
        name: 'Trainroom',
        component: () => import('@/pages/trainroom/index.vue'),
        children: [
            {
                path: '/trainroom/trainright',
                component: () => import('@/pages/trainroom/components/trainRight/index.vue'),
            },
            {
                path: '/trainroom/audio',
                component: () => import('@/pages/trainroom/components/audio/index.vue'),
            },
            {
                path: '/trainroom/cache',
                meta: {
                    title: 'cache',
                },
                component: () => import('@/pages/trainroom/components/cache/index.vue'),
            },
            {
                path: '/trainroom/collect',
                meta: {
                    title: 'collect',
                },
                component: () => import('@/pages/trainroom/components/collect/index.vue'),
            },
            {
                path: '/trainroom/history',
                meta: {
                    title: 'history',
                },
                component: () => import('@/pages/trainroom/components/history/index.vue'),
            },
            {
                path: '/trainroom',
                redirect: '/trainroom/mytrainroom?content=true',
            },
            {
                path: 'mytrainroom',
                component: () => import('@/pages/trainroom/components/mytrainroom/index.vue'),
                children: [
                    {
                        path: 'detialall',
                        component: () => import('@/pages/trainroom/components/detialall/index.vue'),
                    },
                ],
            },
        ],
    },
    // 交流
    {
        path: '/interflow',
        component: () => import('@/pages/interflow/index.vue'),
        meta: {
            name: '交流',
            type: 'interflow',
        },
    },
    {
        path: '/',
        redirect: '/home',
    },
    // 编辑页
    {
        path: '/edit/PublishArticle',
        name: 'Edit',
        component: () => import('@/pages/center/components/rightView/components/edit/index.vue'),
    },
    // 任意路由
    {
        path: '/:pathMatch(.*)',
        name: 'Any',
        redirect: '/404',
    },
];

export default routes;
