import { createApp } from "vue";
import App from "./App.vue";

// 注册pinia
import pinia from "@/store/index";
// 注册路由
import router from "@/router/index";
// 注册element-plus
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import "@/mock/course/course";
import "element-plus/dist/index.css";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
// 全局样式
import "@/styles/index.less";

//引入mock
import "@/trainroommock";

// 注册mock
import "@/mock/index.ts";
// 全局样式
import "@/styles/index.less";

import Header from "@/components/header/index.vue";
import Footer from "@/components/footer/index.vue";
import Interest from "@/components/Interest/index.vue";

// 关联
const app = createApp(App);
// 注册全局组件

app.component("Header", Header);
app.component("Footer", Footer);
app.component("Interest", Interest);

app.use(pinia);
app.use(router);
app.use(ElementPlus);
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component);
}
app.mount("#app");
