import axios, { type AxiosRequestConfig, type AxiosResponse } from 'axios';
import { ElMessage } from 'element-plus';

const request = axios.create({
    baseURL: '/mock',
    timeout: 50000,
});

request.interceptors.request.use((config: AxiosRequestConfig<any>) => {
    return config;
});

request.interceptors.response.use(
    (response: AxiosResponse<any, any>) => {
        return response.data.data;
    },
    error => {
        // 对响应错误做点什么
        if (error.message.indexOf('timeout') != -1) {
            ElMessage.error('网络超时');
        } else if (error.message == 'Network Error') {
            ElMessage.error('网络连接错误');
        } else {
            if (error.response.data) ElMessage.error(error.response.statusText);
            else ElMessage.error('接口路径找不到');
        }
        return Promise.reject(error);
    }
);

export default request;
