import mockquest from '../utils/trainmockrequest';

//初始化页面数据展示定义泛型
export interface initdata {
    canAccess: boolean;
    contentCount: number;
    cover: string;
    createMethod: string;
    creator: string;
    deleteFlag: boolean;
    id: string;
    isDelete: number;
    openOutside: number;
    planAddCount: number;
    planName: string;
    status: string;
    weekTrainedCount: number;
    weekTrainedDuration: number;
    weeklyPlanCount: number;
    weeklyPlanDuration: number;
}

export interface initdataAll {
    content: initdata[];
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    offset: null;
    size: number;
    sort: null;
    statistics: null;
    totalElements: number;
    totalPages: number;
    useOffset: boolean;
}

export type initdatas = initdata[];

//点击左边获取到数据类型--缓存
export interface showpage {
    cacheTime: number;
    cover: string;
    createTime: number;
    creatorBackup: null;
    grantType: string;
    id: string;
    momentCanExtractAudio: true;
    momentId: string;
    operation: null;
    title: string;
    totalDuration: number;
    type: string;
    userId: string;
    videoCount: number;
    videos: [
        {
            cover: string;
            duration: number;
            originUrl: string;
            title: string;
            url: string;
        }
    ];
}

export type showpages = showpage[];

export interface allshowpage {
    content: showpage[];
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    size: number;
    sort: null;
    totalElements: number;
    totalPages: number;
}

//点击左边获取数据类型 --收藏
//一个concent
export interface collectone {
    aiyigeApplication: string;
    createTime: number;
    creator: string;
    id: string;
    momentBackup: {
        cover: string[];
        creator: string;
        mediaSummary: {
            duration: number;
            count: number;
        };
        router: string;
        cityName: string;
        creatorBackup: {
            accid: null;
            avatar: string;
            certificationType: null;
            id: string;
            level: number;
            mobile: null;
            name: string;
            recentSellerFavorableValue: null;
            recomFactor: null;
            recomFactorExpireTime: null;
            sellerFavorableValue: null;
            type: null;
            vipExpireTime: null;
            vipGrade: number;
        };
        cityId: string;
        momentClassificationBackup: {
            workTitle: string;
        };
    };
    momentId: string;
    momentSubject: string;
    momentTitle: string;
    status: number;
}

export interface collect {
    content: collectone[];
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    offset: null;
    size: number;
    sort: null;
    statistics: null;
    totalElements: number;
    totalPages: number;
    useOffset: boolean;
}

export type collects = collect[];

//点击历史浏览数据类型

export interface showhistoryone {
    momentSubject: string;
    creator: string;
    createTime: number;
    momentBackup: {
        cover: string[];
        creator: string;
        router: string;
        creatorBackup: {
            certificationType: string;
            level: number;
            name: string;
            avatar: string;
            id: string;
            vipGrade: number;
        };
        momentClassificationBackup: {
            workTitle: string;
        };
    };
    momentTitle: string;
    updateTime: number;
    id: string;
    momentId: string;
}

export interface showhistoryall {
    content: showhistoryone[];
    first: Boolean;
    last: Boolean;
    number: number;
    numberOfElements: number;
    offset: null;
    size: number;
    sort: null;
    statistics: null;
    totalElements: number;
    totalPages: number;
    useOffset: Boolean;
}
export default {
    //初始化数据--全部
    inittraindata() {
        return mockquest.get<any, initdataAll>('/trainingCenter');
    },
    //缓存
    getshowcache() {
        return mockquest.get<any, allshowpage>('/getuserall');
    },
    //收藏
    getshowcollect() {
        return mockquest.get<any, collect>('/moment/favoriteRecords');
    },
    //历史
    gethistorydata() {
        return mockquest.get<any, showhistoryall>('/moment/viewRecords');
    },
    //我的练习室
    getmytrainroomdata() {
        return mockquest.get<any, initdataAll>('/trainingCenter/plans');
    },
};
