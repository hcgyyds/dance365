import Mock from 'mockjs'
import cacheData from './cache.json'
import collectData from './collect.json'
import history from './history.json'
import mytrain from './mytrain.json'

//初始化数据
Mock.mock('/mock/eShop/refundOrderFees?access_token=59397b7d-705b-4984-830c-f891bfb354c9', function () {
  return {
    code: 200,
    data: [],
    message: 'success'
  }
})
//初始化数据 --全部
Mock.mock('/mock/trainingCenter', function () {
  return {
    code: 200,
    data: mytrain,
    message: 'success'
  }
})

//缓存数据 
Mock.mock('/mock/getuserall', function () {
  return {
    code: 200,
    data: cacheData,
    message: 'success'
  }
})

//收藏
Mock.mock('/mock/moment/favoriteRecords', function () {
  return {
    code: 200,
    data: collectData,
    message: 'success'
  }
})

//历史 
Mock.mock('/mock/moment/viewRecords', function () {
  return {
    code: 200,
    data: history,
    message: 'success'
  }
})
//我的练习室
Mock.mock('/mock/trainingCenter/plans', function () {
  return {
    code: 200,
    data: mytrain,
    message: 'success'
  }
})