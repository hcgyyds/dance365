import { fileURLToPath, URL } from 'node:url';
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [vue()],
    resolve: {
        alias: {
            '@': fileURLToPath(new URL('./src', import.meta.url)),
        },
        extensions: ['.ts', '.vue', '.js', '.jsx', '.tsx'], // 导入时想要省略的扩展名列表。
    },
    server: {
        //配置代理跨域
        proxy: {
            '/api': {
                target: 'http://sph-h5-api.atguigu.cn',
                changeOrigin: true,
                secure: true,
            },
        },
    },
});
